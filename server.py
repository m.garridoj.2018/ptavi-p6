#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import sys
import socketserver
import os
import simplertp
import secrets
import requests


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Metodo para recibir la petición."""
        """Escribe dirección y puerto del cliente (de tupla client_address)."""
        print("Hemos recibido tu peticion ")
    # while 1:
    # Leyendo línea a línea lo que nos envía el cliente
        line = self.rfile.read()
        recibido = line.decode('utf-8').split(" ")
        # print(recibido)
        if recibido != '\r\n':
            Ip_usuario = recibido[1].split('@')[1]
            usuario = recibido[1].split('@')[0].split(':')[1]
            puerto = recibido[1].split('@')[1].split(':')[1]
            otro = recibido[1:]
            sip = otro[0].split(':')[0]
            # print(sip)
            v = "v = 0\r\n"
            o = 'O = ' + usuario + Ip_usuario + '\r\n'
            s = 's = mysession\r\n'
            t = 't = 0\r\n'
            m = 'm = audio ' + puerto + ' RTP\r\n'
            print("El cliente nos manda " + line.decode('utf-8'))
            # print(puerto)
            # print(usuario)
            typo = v + o + s + t + m

            if sip != 'sip':
                self.wfile.write(b" SIP/2.0 400 Bad Request\r\n\r\n")
            elif recibido[0] == 'INVITE':
                mensaje = "SIP/2.0 100 Trying\r\n\r\n"
                # self.wfile.write(mensaje)
                mensaje2 = (mensaje + "SIP / 2.0 180 Ringing\r\n\r\n")
                # self.wfile.write(mensaje2)
                mensaje3 = (mensaje2 + "SIP/2.0 200 OK\r\n\r\n")
                # self.wfile.write(mensaje3)
                longitud = len(typo)
                Contenido = "Content-Lenght = " + str(longitud)+"\r\n"
                mensaje4 = mensaje3 + Contenido + typo
                self.wfile.write(bytes(mensaje4, "utf-8"))
                print('Content-Lenght = ' + str(longitud))
            elif recibido[0] == 'BYE':
                mensaje = b"SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(mensaje)
            elif recibido[0] == 'ACK':
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT, payload_type=14,
                                      ssrc=200002)
                audio = simplertp.RtpPayloadMp3(fichero_audio)
                ip = '127.0.0.1'
                puerto = 23032
                simplertp.send_rtp_packet(RTP_header, audio, ip, puerto)

            elif recibido[0] != ('INVITE', 'BYE', 'ACK'):
                mensaje = b"SIP / 2.0 405 Method Not Allowed\r\n\r\n"
                self.wfile.write(mensaje)

        # for line in self.rfile:
        #    mensaje = line.decode('utf-8')
            # print("El cliente nos manda: ", mensaje)
            # Si no hay más líneas salimos del bucle infinito
            # if not line:
            #    break


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        ip = (sys.argv[1])
        puerto = int(sys.argv[2])
        fichero_audio = sys.argv[3]

    except IndexError:
        sys.exit("Usage: python3 server.py IP port audio_file")
    if len(sys.argv) != 4:
        sys.exit("Usage: python3 server.py IP port audio_file")
    serv = socketserver.UDPServer((ip, puerto), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    print("Listening...")
    try:
        serv.serve_forever()
    except IndexError:
        print('Finalizado servidor')
