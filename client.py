#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

try:
    metodo = sys.argv[1]
    cadena = sys.argv[2]
    receptor = cadena.split("@")[0]
    ip = cadena.split("@")[1].split(":")[0]
    puerto = cadena.split('@')[1].split(':')[1]
except IndexError:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
if len(sys.argv) != 3:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

# Cliente UDP simple.

# Dirección IP del servidor.
# SERVER = 'localhost'
# PORT = 6001
if metodo == "BYE" or metodo == "INVITE":
    LINE = (metodo + " sip:" + cadena + " SIP/2.0\r\n")
else:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((ip, int(puerto)))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    respuesta_servidor1 = data.decode('utf-8')
    if metodo == 'INVITE':
        respuesta_servidor2 = respuesta_servidor1.split("\r\n\r\n")[2]
        print(respuesta_servidor1)
        if respuesta_servidor2 == 'SIP/2.0 200 OK':
            LINE2 = ('ACK' + ' sip:' + cadena + " SIP/2.0")
            print('Enviando ACK...: ' + LINE2)
            my_socket.send(bytes(LINE2, ' utf-8') + b'\r\n')
        # elif respuesta_servidor2 == 'SIP/2.0 100 Trying':
            # LINE2 = ('ACK' + ' sip:' + cadena + " SIP/2.0")
            # print('Enviando ACK...: ' + LINE2)
            # my_socket.send(bytes(LINE2, ' utf-8') + b'\r\n')
        # elif respuesta_servidor2 == 'SIP/2.0 100 Ringing':
            # LINE2 = ('ACK' + ' sip:' + cadena + " SIP/2.0")
            # print('Enviando ACK...: ' + LINE2)
            # my_socket.send(bytes(LINE2, ' utf-8') + b'\r\n')
        # print('Recibido -- ', data.decode('utf-8'))
    print("Terminando socket...")

print("Fin.")
